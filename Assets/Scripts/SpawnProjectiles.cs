﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnProjectiles : MonoBehaviour
{
    [SerializeField] GameObject FirePoint;
    [SerializeField] List<GameObject> Projectiles = new List<GameObject>();
    [SerializeField] float range;
    public RotateToMouse rotateToMouse;
    public float damage = 1f;
    GameObject projectileToSpawn;

    // Start is called before the first frame update
    void Start()
    {
        projectileToSpawn = Projectiles[0];
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetUp(OVRInput.Button.SecondaryIndexTrigger))
        {
            Spawn();
        }
    }

    void Spawn()
    {
        GameObject spawnPoint;

        if(FirePoint != null)
        {
            spawnPoint = Instantiate(projectileToSpawn, FirePoint.transform.position, Quaternion.identity);
            if(rotateToMouse != null)
            {
                spawnPoint.transform.localRotation = rotateToMouse.GetRotation();
            }
        }
        else
        {
            Debug.Log("No Fire Point");
        }

        RaycastHit hit;
        if(Physics.Raycast(FirePoint.transform.position, FirePoint.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);

            Target target = hit.transform.GetComponent<Target>();
            if(target != null)
            {
                target.TakeDamage(damage);
            }
            else{
                Destroy(projectileToSpawn);
            }
        }

    }
}
